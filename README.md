Séance 1:
Installation VM Centos:

Installation des prérequis:
#!/bin/bash
yum --enablerepo=remi,epel install httpd
yum --enablerepo=remi,epel install mysql-server
service mysqld start
/usr/bin/mysql_secure_installation
yum --enablerepo=remi,epel install php php-zip php-mysql php-mcrypt php-xml php-mbstring
service httpd restart
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/bin/composer
chmod +x /usr/bin/composer
cd ~/var/www
git clone https://github.com/laravel/laravel.git
cd
cd ~/var/www/laravel
composer install
cd
chown -R apache.apache /var/www/laravel
chmod -R 755 /var/www/laravel
chmod -R 755 /var/www/laravel/storage
chcon -R -t httpd_sys_rw_content_t /var/www/laravel/storage
cp .env.example .env
php artisan key:generate


emacs /etc/httpd/conf/httpd.conf
création page de test.
service httpd restart

Authentification:
composer require laravel/ui --dev
php artisan ui vue --auth
npm install && npm run dev

Seed Database:
php artisan make:seeder UsersTableSeeder

fichier database seeder:
Créer une boucle tant que i < 100 ajouter un user à la bdd.

composer dump-autoload
php artisan db:seed

Séance 2:

Activation virtualisation dans le BIOS.
Installation vagrant, virtualbox, git et Homestead.
git clone https://github.com/laravel/homestead.git ~/Homestead
CMD->Dossier homestead->vagrant box add laravel/homestead --force
bash init.sh
ssh-keygen -t rsa -C 'moncourriel@mondomaine.com'
Modification des chemins d'accès Laravel.
Modification fichiers hosts
Configuration pour accès mysql a distance via Navicat (GRANT PRIVILEGES)
Transfert du premier projet laravel vers homestead (bdd + fichiers du sites)






